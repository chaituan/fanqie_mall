# 番茄社交电商新零售系统---公众号版

#### 介绍
番茄社交电商新零售系统是一套小程序+公众号H5，移动营销覆盖，结合【分销+评团+砍价+秒杀】 ，拉新引流，社交互动，抢占海量移动电商用户资源的商城系统。
仅供学习，商用请联系开发者授权。

#### 软件架构
Codeigniter + layui + vant-weapp + mysql 

### 友情链接：[社区多门店系统](https://gitee.com/chaituan/fanqie_shop)
### 演示：[前后端演示](https://www.chaituans.com/news/id-11.html)


#### 安装教程

1. 新建数据库，导入根目录下的mall.sql
2. 修改apps->config下的database.php和constants.php文件，分别是数据库链接和公众号appid资料。按里面的提示说明修改即可。
3. 后台密码为 qweqwe

#### 使用说明

本源码是最低版的公众号版本，最新公众号和小程序互通版请往下看。

#### 主要功能点

番茄商城小程序，集成拼团，秒杀，砍价，线下自提，裂变分销，会员等级，积分兑换等多个功能，可以快速帮助中小企业搭建线上商城平台。




![输入图片说明](https://images.gitee.com/uploads/images/2019/0930/152018_b6815afd_430822.jpeg "qrcode_for_gh_014c66f6d601_258.jpg")